
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mansoor-pt2603
 */
import java.util.*;

public class Employee {

    Scanner sc = new Scanner(System.in);
    Scanner st = new Scanner(System.in);
    int id;
    String name;
    int age;
    String Department;
    String Designation;
    String ReportingTo;

    public Employee() {
    }

    public Employee(int id, String name, int age, String Department, String Designation, String ReportingTO) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.Department = Department;
        this.Designation = Designation;
        this.ReportingTo = ReportingTO;
    }

    void showAllRecords(ArrayList<Employee> list) {
        System.out.println("________________________________________________________________________________________________________");
        System.out.format("%-5s| %-20s| %-5s| %-20s| %-20s| %-20s |", "ID", "NAME", "AGE", "DEPARTMENT", "DESIGNATION", "REPORTINGTO");
        System.out.println("");
        System.out.println("________________________________________________________________________________________________________");
        for (Employee i : list) {
            System.out.format("%-5d| %-20s| %-5d| %-20s| %-20s| %-20s |", i.id, i.name, i.age, i.Department, i.Designation, i.ReportingTo);
            System.out.println("");
        }
        System.out.println("________________________________________________________________________________________________________");
    }

    void searchRecords(ArrayList<Employee> list) {

        here:
        while (true) {
            System.out.println("choose the option below to search by:");
            System.out.println("\n1.By Name \n2.Age \n3.Department \n4.Designation \n5.ReportingTo \n6.Exit");
            int n = sc.nextInt();

            switch (n) {
                case 1:
                    System.out.println("enter the name to display the records");
                    String name = st.next();
                    System.out.println("________________________________________________________________________________________________________");
                    System.out.format("%-5s| %-20s| %-5s| %-20s| %-20s| %-20s |", "ID", "NAME", "AGE", "DEPARTMENT", "DESIGNATION", "REPORTINGTO");
                    System.out.println("");
                    System.out.println("________________________________________________________________________________________________________");
                    for (Employee x : list) {
                        if (name.equalsIgnoreCase(x.name)) {
                            System.out.format("%-5d| %-20s| %-5d| %-20s| %-20s| %-20s |", x.id, x.name, x.age, x.Department, x.Designation, x.ReportingTo);
                            System.out.println("");
                        }
                    }
                    break;
                case 2:
                    System.out.println("enter the Age to display the records");
                    String age = sc.next();
                    System.out.println("________________________________________________________________________________________________________");
                    System.out.format("%-5s| %-20s| %-5s| %-20s| %-20s| %-20s |", "ID", "NAME", "AGE", "DEPARTMENT", "DESIGNATION", "REPORTINGTO");
                    System.out.println("");
                    System.out.println("________________________________________________________________________________________________________");
                    for (Employee x : list) {
                        String sage = "" + x.age;
                        if (sage.contains(age)) {
                            System.out.format("%-5d| %-20s| %-5d| %-20s| %-20s| %-20s |", x.id, x.name, x.age, x.Department, x.Designation, x.ReportingTo);
                            System.out.println("");
                        }
                    }
                    break;
                case 3:
                    System.out.println("enter the Department name to display the records");
                    String Department = st.next().toLowerCase();
                    System.out.println("________________________________________________________________________________________________________");
                    System.out.format("%-5s| %-20s| %-5s| %-20s| %-20s| %-20s |", "ID", "NAME", "AGE", "DEPARTMENT", "DESIGNATION", "REPORTINGTO");
                    System.out.println("");
                    System.out.println("________________________________________________________________________________________________________");
                    for (Employee x : list) {

                        if (x.Department.toLowerCase().contains(Department)) {
                            System.out.format("%-5d| %-20s| %-5d| %-20s| %-20s| %-20s |", x.id, x.name, x.age, x.Department, x.Designation, x.ReportingTo);
                            System.out.println("");
                        }
                    }
                    break;
                case 4:
                    System.out.println("enter the Designation name to display the records");
                    String Designation = st.next();
                    System.out.println("________________________________________________________________________________________________________");
                    System.out.format("%-5s| %-20s| %-5s| %-20s| %-20s| %-20s |", "ID", "NAME", "AGE", "DEPARTMENT", "DESIGNATION", "REPORTINGTO");
                    System.out.println("");
                    System.out.println("________________________________________________________________________________________________________");
                    for (Employee x : list) {
                        if (Designation.equalsIgnoreCase(x.Designation)) {
                            System.out.format("%-5d| %-20s| %-5d| %-20s| %-20s| %-20s |", x.id, x.name, x.age, x.Department, x.Designation, x.ReportingTo);
                            System.out.println("");
                        }
                    }
                    break;
                case 5:
                    System.out.println("enter the ReportingTo authority name  display the records");
                    String ReportingTo = st.next().toLowerCase();
                    System.out.println("________________________________________________________________________________________________________");
                    System.out.format("%-5s| %-20s| %-5s| %-20s| %-20s| %-20s |", "ID", "NAME", "AGE", "DEPARTMENT", "DESIGNATION", "REPORTINGTO");
                    System.out.println("");
                    System.out.println("________________________________________________________________________________________________________");
                    for (Employee x : list) {
                        if (x.ReportingTo.toLowerCase().contains(ReportingTo)) {
                            System.out.format("%-5d| %-20s| %-5d| %-20s| %-20s| %-20s |", x.id, x.name, x.age, x.Department, x.Designation, x.ReportingTo);
                            System.out.println("");
                        }
                    }
                    break;
                case 6:
                    break here;
            }
        }
    }

    void managingReport(ArrayList<Employee> list, String managerName) {
        for (Employee x : list) {
            if (managerName.equalsIgnoreCase(x.ReportingTo)) {
                System.out.println(x.name);

            }
        }
    }

    void reportingTree(ArrayList<Employee> list, String name) {
        System.out.print(name + "-->");
        for (Employee y : list) {
            for (Employee x : list) {
                if (x.name.equalsIgnoreCase(name)) {
                    System.out.print(x.ReportingTo + "-->");
                    name = x.ReportingTo;
                    break;
                }
            }
        }
    }

    void summaryReport(ArrayList<Employee> list) {
        here:
        while (true) {
            System.out.println("\n1.By Department\n2.By Designation \n3.By Manager Name\n4.Exit");
            int num = sc.nextInt();

            switch (num) {
                case 1:
                    HashMap<String, Integer> map = new HashMap<String, Integer>();
                    for (Employee x : list) {
                        if (map.containsKey(x.Department)) {
                            map.put(x.Department, map.get(x.Department) + 1);
                        } else {
                            map.put(x.Department, 1);
                        }

                    }
                    for (String department : map.keySet()) {
                        System.out.println(department + " : " + map.get(department));
                    }
                    break;
                case 2:
                    HashMap<String, Integer> map1 = new HashMap<String, Integer>();
                    for (Employee x : list) {
                        if (map1.containsKey(x.Designation)) {
                            map1.put(x.Designation, map1.get(x.Designation) + 1);
                        } else {
                            map1.put(x.Designation, 1);
                        }

                    }
                    for (String Designation : map1.keySet()) {
                        System.out.println(Designation + " : " + map1.get(Designation));
                    }
                    break;
                case 3:
                    HashMap<String, Integer> map2 = new HashMap<String, Integer>();
                    for (Employee x : list) {
                        if (map2.containsKey(x.ReportingTo)) {
                            map2.put(x.ReportingTo, map2.get(x.ReportingTo) + 1);
                        } else {
                            map2.put(x.ReportingTo, 1);
                        }

                    }
                    for (String Manager : map2.keySet()) {
                        if (!Manager.equals("___")) {
                            System.out.println(Manager + " : " + map2.get(Manager));
                        }
                    }
                    break;
                case 4:
                    break here;
            }

        }
    }
}
