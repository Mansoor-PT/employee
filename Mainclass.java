
import java.util.*;

public class Mainclass {

    public static void main(String[] args) {
        Employee obj = new Employee();
        ArrayList<Employee> list = new ArrayList<Employee>();
        list.add(new Employee(1, "Sriram", 45, "Management", "CEO", "___"));
        list.add(new Employee(2, "Mukund", 42, "HR", "HR Manager", "Sriram"));
        list.add(new Employee(3, "Sebastian", 38, "FInance", "Finance Manager", "Sriram"));
        list.add(new Employee(4, "Aashritha", 32, "Product Management", "Dev Manager", "Sriram"));
        list.add(new Employee(5, "Mohammad Rafi", 35, "HR", "HR Lead", "Mukund"));
        list.add(new Employee(6, "Anjali Kumar", 29, "HR", "HR Assocuate", "Mohammad Rafi"));
        list.add(new Employee(7, "Joseph", 40, "FInance", "Finance Associate", "Sebastian"));
        list.add(new Employee(8, "Ramachandran", 27, "Product Development", "Tech Lead", "Aashritha"));
        list.add(new Employee(9, "Abhinaya shankar", 23, "Product Development", "System Developer", "Ramachandran"));
        list.add(new Employee(10, "Imran Khan", 28, "Product Testing", "QA Lead", "Ramachandran"));
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        here:
        while (flag) {
            System.out.println("Enter the options below:" + "\n1.show all the Employee details \n2.Search Records \n3.Manager Report \n4.Reporting Tree\n5.Summary Report");
            int n = sc.nextInt();
            sc.nextLine();
            switch (n) {
                case 1:
                    obj.showAllRecords(list);
                  break;
                case 2:
                    obj.searchRecords(list);
                    break;
                case 3:
                    System.out.println("enter the name to be searched for:");
                    String name = sc.next();
                    obj.managingReport(list, name);
                    break;
                case 4:
                    System.out.println("enter the name to be searched for:");
                    String reportingName = sc.nextLine();
                    obj.reportingTree(list, reportingName.toLowerCase());
                    break;
                case 5:
                    obj.summaryReport(list);
                case 6:
                    break here;
            }
        }
    }
}
